// package: pay90.proto
// file: market.proto

import * as market_pb from "./market_pb";
import {grpc} from "@improbable-eng/grpc-web";

type PublicCheck = {
  readonly methodName: string;
  readonly service: typeof Public;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof market_pb.Empty;
  readonly responseType: typeof market_pb.Empty;
};

type PublicGetCurrencies = {
  readonly methodName: string;
  readonly service: typeof Public;
  readonly requestStream: false;
  readonly responseStream: true;
  readonly requestType: typeof market_pb.Empty;
  readonly responseType: typeof market_pb.Currency;
};

type PublicGetProviders = {
  readonly methodName: string;
  readonly service: typeof Public;
  readonly requestStream: false;
  readonly responseStream: true;
  readonly requestType: typeof market_pb.Empty;
  readonly responseType: typeof market_pb.Provider;
};

type PublicCreateOrder = {
  readonly methodName: string;
  readonly service: typeof Public;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof market_pb.Order;
  readonly responseType: typeof market_pb.Order;
};

type PublicGetOrder = {
  readonly methodName: string;
  readonly service: typeof Public;
  readonly requestStream: false;
  readonly responseStream: true;
  readonly requestType: typeof market_pb.Order;
  readonly responseType: typeof market_pb.Order;
};

type PublicGetOrders = {
  readonly methodName: string;
  readonly service: typeof Public;
  readonly requestStream: false;
  readonly responseStream: true;
  readonly requestType: typeof market_pb.Empty;
  readonly responseType: typeof market_pb.Order;
};

type PublicGetMessages = {
  readonly methodName: string;
  readonly service: typeof Public;
  readonly requestStream: false;
  readonly responseStream: true;
  readonly requestType: typeof market_pb.Empty;
  readonly responseType: typeof market_pb.Message;
};

type PublicSendMessage = {
  readonly methodName: string;
  readonly service: typeof Public;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof market_pb.Message;
  readonly responseType: typeof market_pb.Empty;
};

export class Public {
  static readonly serviceName: string;
  static readonly Check: PublicCheck;
  static readonly GetCurrencies: PublicGetCurrencies;
  static readonly GetProviders: PublicGetProviders;
  static readonly CreateOrder: PublicCreateOrder;
  static readonly GetOrder: PublicGetOrder;
  static readonly GetOrders: PublicGetOrders;
  static readonly GetMessages: PublicGetMessages;
  static readonly SendMessage: PublicSendMessage;
}

export class Private {
  static readonly serviceName: string;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class PublicClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  check(
    requestMessage: market_pb.Empty,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: market_pb.Empty|null) => void
  ): UnaryResponse;
  check(
    requestMessage: market_pb.Empty,
    callback: (error: ServiceError|null, responseMessage: market_pb.Empty|null) => void
  ): UnaryResponse;
  getCurrencies(requestMessage: market_pb.Empty, metadata?: grpc.Metadata): ResponseStream<market_pb.Currency>;
  getProviders(requestMessage: market_pb.Empty, metadata?: grpc.Metadata): ResponseStream<market_pb.Provider>;
  createOrder(
    requestMessage: market_pb.Order,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: market_pb.Order|null) => void
  ): UnaryResponse;
  createOrder(
    requestMessage: market_pb.Order,
    callback: (error: ServiceError|null, responseMessage: market_pb.Order|null) => void
  ): UnaryResponse;
  getOrder(requestMessage: market_pb.Order, metadata?: grpc.Metadata): ResponseStream<market_pb.Order>;
  getOrders(requestMessage: market_pb.Empty, metadata?: grpc.Metadata): ResponseStream<market_pb.Order>;
  getMessages(requestMessage: market_pb.Empty, metadata?: grpc.Metadata): ResponseStream<market_pb.Message>;
  sendMessage(
    requestMessage: market_pb.Message,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: market_pb.Empty|null) => void
  ): UnaryResponse;
  sendMessage(
    requestMessage: market_pb.Message,
    callback: (error: ServiceError|null, responseMessage: market_pb.Empty|null) => void
  ): UnaryResponse;
}

export class PrivateClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
}

