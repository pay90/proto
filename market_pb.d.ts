// package: pay90.proto
// file: market.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";

export class Empty extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Empty.AsObject;
  static toObject(includeInstance: boolean, msg: Empty): Empty.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Empty, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Empty;
  static deserializeBinaryFromReader(message: Empty, reader: jspb.BinaryReader): Empty;
}

export namespace Empty {
  export type AsObject = {
  }
}

export class Direction extends jspb.Message {
  getShortName(): string;
  setShortName(value: string): void;

  getRate(): number;
  setRate(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Direction.AsObject;
  static toObject(includeInstance: boolean, msg: Direction): Direction.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Direction, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Direction;
  static deserializeBinaryFromReader(message: Direction, reader: jspb.BinaryReader): Direction;
}

export namespace Direction {
  export type AsObject = {
    shortName: string,
    rate: number,
  }
}

export class Card extends jspb.Message {
  getNumber(): string;
  setNumber(value: string): void;

  getNumberRegexp(): string;
  setNumberRegexp(value: string): void;

  getHolder(): string;
  setHolder(value: string): void;

  getHolderRegexp(): string;
  setHolderRegexp(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Card.AsObject;
  static toObject(includeInstance: boolean, msg: Card): Card.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Card, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Card;
  static deserializeBinaryFromReader(message: Card, reader: jspb.BinaryReader): Card;
}

export namespace Card {
  export type AsObject = {
    number: string,
    numberRegexp: string,
    holder: string,
    holderRegexp: string,
  }
}

export class Crypto extends jspb.Message {
  getAddress(): string;
  setAddress(value: string): void;

  getAddressRegexp(): string;
  setAddressRegexp(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Crypto.AsObject;
  static toObject(includeInstance: boolean, msg: Crypto): Crypto.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Crypto, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Crypto;
  static deserializeBinaryFromReader(message: Crypto, reader: jspb.BinaryReader): Crypto;
}

export namespace Crypto {
  export type AsObject = {
    address: string,
    addressRegexp: string,
  }
}

export class Mobile extends jspb.Message {
  getProvider(): string;
  setProvider(value: string): void;

  getPhoneNumber(): string;
  setPhoneNumber(value: string): void;

  getPhoneNumberRegexp(): string;
  setPhoneNumberRegexp(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Mobile.AsObject;
  static toObject(includeInstance: boolean, msg: Mobile): Mobile.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Mobile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Mobile;
  static deserializeBinaryFromReader(message: Mobile, reader: jspb.BinaryReader): Mobile;
}

export namespace Mobile {
  export type AsObject = {
    provider: string,
    phoneNumber: string,
    phoneNumberRegexp: string,
  }
}

export class Provider extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getIcon(): string;
  setIcon(value: string): void;

  getComment(): string;
  setComment(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Provider.AsObject;
  static toObject(includeInstance: boolean, msg: Provider): Provider.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Provider, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Provider;
  static deserializeBinaryFromReader(message: Provider, reader: jspb.BinaryReader): Provider;
}

export namespace Provider {
  export type AsObject = {
    name: string,
    icon: string,
    comment: string,
  }
}

export class Contact extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getMail(): string;
  setMail(value: string): void;

  getOrderId(): string;
  setOrderId(value: string): void;

  getMessage(): string;
  setMessage(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Contact.AsObject;
  static toObject(includeInstance: boolean, msg: Contact): Contact.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Contact, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Contact;
  static deserializeBinaryFromReader(message: Contact, reader: jspb.BinaryReader): Contact;
}

export namespace Contact {
  export type AsObject = {
    name: string,
    mail: string,
    orderId: string,
    message: string,
  }
}

export class Currency extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getShortName(): string;
  setShortName(value: string): void;

  getIcon(): string;
  setIcon(value: string): void;

  clearDirectionsList(): void;
  getDirectionsList(): Array<Direction>;
  setDirectionsList(value: Array<Direction>): void;
  addDirections(value?: Direction, index?: number): Direction;

  getAmountMin(): number;
  setAmountMin(value: number): void;

  getAmountMax(): number;
  setAmountMax(value: number): void;

  getAmount(): number;
  setAmount(value: number): void;

  getType(): Currency.TypeMap[keyof Currency.TypeMap];
  setType(value: Currency.TypeMap[keyof Currency.TypeMap]): void;

  hasCard(): boolean;
  clearCard(): void;
  getCard(): Card | undefined;
  setCard(value?: Card): void;

  hasCrypto(): boolean;
  clearCrypto(): void;
  getCrypto(): Crypto | undefined;
  setCrypto(value?: Crypto): void;

  hasMobile(): boolean;
  clearMobile(): void;
  getMobile(): Mobile | undefined;
  setMobile(value?: Mobile): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Currency.AsObject;
  static toObject(includeInstance: boolean, msg: Currency): Currency.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Currency, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Currency;
  static deserializeBinaryFromReader(message: Currency, reader: jspb.BinaryReader): Currency;
}

export namespace Currency {
  export type AsObject = {
    name: string,
    shortName: string,
    icon: string,
    directionsList: Array<Direction.AsObject>,
    amountMin: number,
    amountMax: number,
    amount: number,
    type: Currency.TypeMap[keyof Currency.TypeMap],
    card?: Card.AsObject,
    crypto?: Crypto.AsObject,
    mobile?: Mobile.AsObject,
  }

  export interface TypeMap {
    CARD: 0;
    CRYPTO: 1;
    MOBILE: 2;
  }

  export const Type: TypeMap;
}

export class OrderStatus extends jspb.Message {
  getStatus(): StatusMap[keyof StatusMap];
  setStatus(value: StatusMap[keyof StatusMap]): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getComment(): string;
  setComment(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OrderStatus.AsObject;
  static toObject(includeInstance: boolean, msg: OrderStatus): OrderStatus.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OrderStatus, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OrderStatus;
  static deserializeBinaryFromReader(message: OrderStatus, reader: jspb.BinaryReader): OrderStatus;
}

export namespace OrderStatus {
  export type AsObject = {
    status: StatusMap[keyof StatusMap],
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    comment: string,
  }
}

export class Order extends jspb.Message {
  hasFrom(): boolean;
  clearFrom(): void;
  getFrom(): Currency | undefined;
  setFrom(value?: Currency): void;

  getFromAmount(): number;
  setFromAmount(value: number): void;

  hasTo(): boolean;
  clearTo(): void;
  getTo(): Currency | undefined;
  setTo(value?: Currency): void;

  getToAmount(): number;
  setToAmount(value: number): void;

  getRate(): number;
  setRate(value: number): void;

  getId(): number;
  setId(value: number): void;

  clearStatusList(): void;
  getStatusList(): Array<OrderStatus>;
  setStatusList(value: Array<OrderStatus>): void;
  addStatus(value?: OrderStatus, index?: number): OrderStatus;

  getComment(): string;
  setComment(value: string): void;

  getEmail(): string;
  setEmail(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Order.AsObject;
  static toObject(includeInstance: boolean, msg: Order): Order.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Order, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Order;
  static deserializeBinaryFromReader(message: Order, reader: jspb.BinaryReader): Order;
}

export namespace Order {
  export type AsObject = {
    from?: Currency.AsObject,
    fromAmount: number,
    to?: Currency.AsObject,
    toAmount: number,
    rate: number,
    id: number,
    statusList: Array<OrderStatus.AsObject>,
    comment: string,
    email: string,
  }
}

export class Sender extends jspb.Message {
  getName(): string;
  setName(value: string): void;

  getAvatar(): string;
  setAvatar(value: string): void;

  getType(): Sender.STypeMap[keyof Sender.STypeMap];
  setType(value: Sender.STypeMap[keyof Sender.STypeMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Sender.AsObject;
  static toObject(includeInstance: boolean, msg: Sender): Sender.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Sender, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Sender;
  static deserializeBinaryFromReader(message: Sender, reader: jspb.BinaryReader): Sender;
}

export namespace Sender {
  export type AsObject = {
    name: string,
    avatar: string,
    type: Sender.STypeMap[keyof Sender.STypeMap],
  }

  export interface STypeMap {
    SUPPORT: 0;
    CLIENT: 1;
    SYSTEM: 2;
  }

  export const SType: STypeMap;
}

export class Message extends jspb.Message {
  hasSender(): boolean;
  clearSender(): void;
  getSender(): Sender | undefined;
  setSender(value?: Sender): void;

  getText(): string;
  setText(value: string): void;

  getMimo(): string;
  setMimo(value: string): void;

  getFile(): Uint8Array | string;
  getFile_asU8(): Uint8Array;
  getFile_asB64(): string;
  setFile(value: Uint8Array | string): void;

  getFilename(): string;
  setFilename(value: string): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getOrderId(): string;
  setOrderId(value: string): void;

  getSended(): boolean;
  setSended(value: boolean): void;

  getReaded(): boolean;
  setReaded(value: boolean): void;

  getId(): string;
  setId(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Message.AsObject;
  static toObject(includeInstance: boolean, msg: Message): Message.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Message, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Message;
  static deserializeBinaryFromReader(message: Message, reader: jspb.BinaryReader): Message;
}

export namespace Message {
  export type AsObject = {
    sender?: Sender.AsObject,
    text: string,
    mimo: string,
    file: Uint8Array | string,
    filename: string,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    orderId: string,
    sended: boolean,
    readed: boolean,
    id: string,
  }
}

export interface StatusMap {
  NEW: 0;
  PROCESSING: 1;
  CLIENT_MARK_AS_SENT: 2;
  PAYMENT_RECIEVED: 3;
  PAYMENT_SENT: 4;
  CLIENT_MARK_AS_RECIEVED: 5;
  CLOSED: 6;
}

export const Status: StatusMap;

